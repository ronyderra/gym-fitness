<?php $themePath = get_template_directory_uri(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head(); ?>
</head>

<body>
    <header class="site-header">

        <!--container-->
        <div class="container header-grid">

            <!--navigation bar-->
            <div class="navigation-bar">

                <!--logo-->
                <div class="logo">
                    <a href="<?php home_url(); ?>">
                        <img src="<?php echo $themePath . "/img/logo.svg" ?>" alt="Site logo" />
                    </a>
                </div>

                <!-- ToDo menu -->
                <?php
                $args = array(
                    'theme_location' => 'main-menu',
                    'container' => 'nav',
                    'container_class' => 'main-menu'
                );
                wp_nav_menu($args);
                ?>

            </div>

        </div>
    </header>